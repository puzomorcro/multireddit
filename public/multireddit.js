function toggleVisibility(id)
{
  var e = document.getElementById(id);
  e.style.display = e.style.display === "none" ? "" : "none";
}

function toggleContent(elem)
{
  elem.innerText = elem.innerText === "Show" ? "Hide" : "Show";
}

function embed(id, src, tag)
{
  document.getElementById(id).getElementsByTagName(tag)[0].src = src;
}

function toggleNSFW()
{
  [...document.getElementsByClassName("thumb")].forEach(function (elem) {
    elem.classList.toggle('nsfw');
  });
}

function htmlUnescape(text)
{
  var doc = new DOMParser().parseFromString(text, 'text/html');
  return doc.documentElement.textContent;
}

function RedditParse(response)
{
  var main = document.body.getElementsByClassName('main')[0]
  var controlsHTML = document.getElementById('controls').outerHTML
  main.innerHTML = controlsHTML
  var num_items = +response['data']['dist'];
  var item;

  for (item of response['data']['children']) {
    var itemData = item['data'];
    var article = document.createElement('article');

    var content_top = document.createElement('div');
    content_top.classList.add('content_top');

    var span = document.createElement('span');
    content_top.appendChild(span);

    var header = document.createElement('span');
    header.innerHTML = "+" + itemData['ups'] + ' <b><a target=_blank href=https://reddit.com' + itemData['permalink'] + '>' + itemData['subreddit'] + '</a>';
    var title = document.createElement('p');
    title.innerHTML = "<b>" + itemData['title'] + "</b>";

    var content = document.createElement('div');
    content.classList.add('content');

    content_top.appendChild(title);
    content.appendChild(content_top);
    var url = document.createElement('a');
    url.target = "_blank";
    url.href = itemData['url'];
    url.innerHTML = itemData['url'];
    content.appendChild(url);

    var is_crossposted = 'crosspost_parent_list' in itemData;
    var media_holder = itemData;
    if (is_crossposted) {
      media_holder = itemData['crosspost_parent_list'][0];
    }

    var embed_button = null;
    if ("media" in media_holder && media_holder["media"]) {
      if ("oembed" in media_holder["media"]) {
        var embed_content = media_holder["media"]['oembed'];
        var embed_elem = document.createElement('div');
        embed_elem.id = media_holder['id'];
        embed_elem.style = "display: none;";
        embed_elem.classList.add('embed', 'indent');
        embed_elem.innerHTML = htmlUnescape(embed_content['html']);

        var iframe = embed_elem.getElementsByTagName('iframe')[0];
        var iframe_src = iframe.src;
        iframe.style += "width: 100%;";
        iframe.src = "";

        embed_button = document.createElement('button');
        embed_button.innerHTML = "Show";
        embed_button.onclick = function(id, src) {
          return function() {
            toggleVisibility(id + '_thumb');
            toggleVisibility(id);
            toggleContent(this);
            embed(id, src, 'iframe');
            document.getElementById(id).parentNode.parentNode.scrollIntoView();
          };
        }(media_holder['id'], iframe_src);

        content.appendChild(embed_elem);
        span.appendChild(embed_button);
      } else if ('reddit_video' in media_holder['media']) {
        var embed_content = media_holder["media"]['reddit_video'];
        var embed_elem = document.createElement('div');
        embed_elem.id = media_holder['id'];
        embed_elem.style = "display: none;";
        embed_elem.classList.add('embed', 'indent');
        var video = document.createElement('video');
        video.controls = true;
        video.style = "width: 100%;";
        embed_elem.appendChild(video);

        embed_button = document.createElement('button');
        embed_button.innerHTML = "Show";
        embed_button.onclick = function(id, src) {
          return function() {
            toggleVisibility(id + '_thumb');
            toggleVisibility(id);
            toggleContent(this);
            embed(id, src, 'video');
            document.getElementById(id).parentNode.parentNode.scrollIntoView();
          };
        }(media_holder['id'], media_holder['media']['reddit_video']['fallback_url']);

        content.appendChild(embed_elem);
        span.appendChild(embed_button);
      }
    }

    if (media_holder['selftext_html']) {
      var indent = document.createElement('div');
      indent.classList.add('indent');
      indent.innerHTML = htmlUnescape(media_holder['selftext_html']);
      indent.style = "width: 100%; display: none;";
      indent.id = media_holder['id'];

      content_top.appendChild(indent);

      if (!embed_button) {
        embed_button = document.createElement('button');
        embed_button.innerHTML = "Show";
        span.appendChild(embed_button);
      }

      embed_button.onclick = function(id) {
        return function() {
          toggleVisibility(id);
          toggleContent(this);
          document.getElementById(id).parentNode.parentNode.scrollIntoView();
        };
      }(media_holder['id']);

    } else if ('post_hint' in media_holder) {
      if (media_holder['post_hint'] == 'image') {
        var indent = document.createElement('div');
        indent.classList.add('indent');
        indent.id = media_holder['id'];
        indent.style = 'display: none;';
        var img = document.createElement('img');
        img.style = "width: 100%;";
        indent.appendChild(img);
        content_top.appendChild(indent);

        if (!embed_button) {
          embed_button = document.createElement('button');
          embed_button.innerHTML = "Show";
          span.appendChild(embed_button);
        }

        embed_button.onclick = function(id, src) {
          return function() {
            toggleVisibility(id + '_thumb');
            toggleVisibility(id);
            toggleContent(this);
            embed(id, src, 'img');
            document.getElementById(id).parentNode.parentNode.scrollIntoView();
          };
        }(media_holder['id'], media_holder['url']);
      }
    }

    if (is_crossposted) {
      header.innerHTML += ' ↚  <a target="_blank" href="https://reddit.com' + media_holder['permalink'] + '">' + media_holder['subreddit'] + '</a>';
    }
    header.innerHTML += '</b>';
    article.appendChild(content);

    if (itemData['thumbnail'] && itemData['thumbnail'] != 'self' && itemData['thumbnail'] != 'default') {
      var thumbnail = document.createElement('div');
      thumbnail.classList.add('img_holder');
      thumbnail.id = media_holder['id'] + '_thumb';
      var img = document.createElement('img');
      img.src = itemData['thumbnail'];
      img.width = itemData['thumbnail_width'];
      img.height = itemData['thumbnail_height'];
      img.classList.add('thumb');
      thumbnail.appendChild(img);
      article.appendChild(thumbnail);
    }

    span.appendChild(header);
    main.appendChild(article);
  }
}

function getNewContent()
{
  var newScript = document.createElement('script')
  var query = document.getElementById('subreddit').value;
  newScript.src = "https://www.reddit.com/" + query + "/top.json?&limit=100&t=week&show=all&jsonp=RedditParse"
  document.head.appendChild(newScript);
  newScript.parentNode.removeChild(newScript);
}
